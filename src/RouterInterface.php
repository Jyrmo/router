<?php

namespace Jyrmo\Router;

interface RouterInterface
{
	/**
	 * @throws Exception\RouterException
	 */
	public function route(RequestInterface $request) : ResponseInterface;
}

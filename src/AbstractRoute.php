<?php

namespace Jyrmo\Router;

abstract class AbstractRoute implements RouteInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function getResponse()
    {
        return $this->response;
    }
}

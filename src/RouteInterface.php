<?php

namespace Jyrmo\Router;

interface RouteInterface
{
    public function setRequest(RequestInterface $request);

    public function beforeDispatch();

    public function dispatch();

    public function afterDispatch();

    public function getResponse();
}

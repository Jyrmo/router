<?php

namespace Jyrmo\Router\Exception;

class CannotResolveRouteException extends RouterException
{}

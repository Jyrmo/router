<?php

namespace Jyrmo\Router;

interface RouteResolverInterface
{
	/**
	 * @throws Exception\CannotResolveRouteException
	 */
	public function resolve(RequestInterface $request) : RouteInterface;
}

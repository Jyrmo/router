<?php

namespace Jyrmo\Router;

class Router implements RouterInterface
{
	/**
	 * @var RouteResolverInterface
	 */
	protected $routeResolver;

	public function setRouteResolver(RouteResolverInterface $routeResolver)
    {
		$this->routeResolver = $routeResolver;
	}

	public function getRouteResolver() : RouteResolverInterface {
		return $this->routeResolver;
	}

	public function __construct(RouteResolverInterface $routeResolver)
    {
		$this->routeResolver = $routeResolver;
	}

	public function route(RequestInterface $request) : ResponseInterface
    {
		$route = $this->routeResolver->resolve($request);
        $route->setRequest($request);
        $route->beforeDispatch();
        $route->dispatch();
        $route->afterDispatch();
        $response = $route->getResponse();

		return $response;
	}
}
